# -*- coding: utf-8 -*-
# -*- mode: org -*-

#+TITLE: Companion data
#+AUTHOR: Alles et al.

#+STARTUP: overview indent

This directory contains the data necessary to run the [[../scripts/][scripts]] that
generate figures. The table below contains a short description of each
dataset.

| File name                             | Description                                                                                                     |
|---------------------------------------+-----------------------------------------------------------------------------------------------------------------|
| [[tky-2006-mean-gcc.csv]]                 | Daily mean Gcc values for the year of 2006 in the TKY dataset                                                   |
| [[tky-2006-mean-rcc.csv]]                 | Daily mean Rcc values for the year of 2006 in the TKY dataset                                                   |
| [[ahs.csv.gz]]                            | Histograms with the metrics (Gcc, HSV) and metadata extracted from the AHS dataset                              |
| [[mtk.csv.gz]]                            | Histograms with the metrics (Gcc, HSV) and metadata extracted from the MTK dataset                              |
| [[tky-partial.csv.gz]]                    | Partial histograms with the metrics (Gcc, HSV) and metatada extracted from the TKY dataset                      |
| [[tky.csv.gz]]                            | Histograms with the metrics (Gcc, HSV) and metadata extracted from the TKY dataset                              |
| [[ahs-experiment.csv]]                    | Performance analysis data (number of cores, execution time) for the AHS dataset                                 |
| [[tky-experiment.csv]]                    | Performance analysis data (number of cores, execution time) for the TKY dataset                                 |
| [[mtk-experiment.csv]]                    | Performance analysis data (number of cores, execution time) for the MTK dataset                                 |
| [[mtk-nomask-experiment.csv]]             | Performance analysis data (number of cores, execution time) for the analysis of masked and unmasked performance |
| [[dc_TKY_y18_n_2015_grain100_masked.csv]] | Histograms with PhenoVis metrics for the TKY dataset                                                            |
| [[phenovis.palette]]                      | Fixed color palette for the PhenoVis CPM                                                                        |
