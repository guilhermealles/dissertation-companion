#+/bin/bash

echo 'Creating Figures from Chapter 2...'
cd chapter-2
for file in $(find . | grep "\.R$") ; do
  Rscript $file
done

echo 'Creating Figures from Chapter 4...'
cd ../chapter-4
for file in $(find . | grep "\.R$") ; do
  Rscript $file
done

echo 'Creating Figures from Chapter 6...'
cd ../chapter-6
for file in $(find . | grep "\.R$") ; do
  Rscript $file
done

echo 'Done.'
